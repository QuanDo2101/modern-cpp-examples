#include <iostream>
#include <string>

using namespace std;

int main()
{
   // Uniform initialisation {.....}
   string s1{"This is text"};

   // default ctor
   string s2;

   // string::capacity() returns the size of the storage space currently
   // allocated for the string, expressed in terms of bytes.
   // Both string::size() and string::length() are synonyms and return the
   // exact same value.
   cout << "Capacity s1 = " << s1.capacity() << " size = " << s1.size() << "  '"
        << s1 << "'" << endl;
   cout << "Capacity s2 = " << s2.capacity() << " size = " << s2.size() << "  '"
        << s2 << "'" << endl
        << endl;

   s2 += "123";
   cout << "Capacity s2 = " << s2.capacity() << " size = " << s2.size() << "  '"
        << s2 << "'" << endl
        << endl;

   s2 = "This is a very long string containing a lot of characters.";
   cout << "Capacity s2 = " << s2.capacity() << " size = " << s2.size() << "  '"
        << s2 << "'" << endl
        << endl;

   s2.clear();
   cout << "Capacity s2 = " << s2.capacity() << " size = " << s2.size() << "  '"
        << s2 << "'" << endl
        << endl;

   s2 = s1;
   cout << "Capacity s2 = " << s2.capacity() << " size = " << s2.size() << "  '"
        << s2 << "'" << endl;

   cout << "Substring s2 = " << s2.substr(1, 3) << endl << endl;
   cout << "Capacity s2 = " << s2.capacity() << " size = " << s2.size() << "  '"
        << s2 << "'" << endl
        << endl;

   // An anonymous object (object without a name) is assigned to s2.
   s2 = string(100, '-');
   cout << "Capacity s2 = " << s2.capacity() << " size = " << s2.size() << "  '"
        << s2 << "'" << endl
        << endl;
        
   s2.append("more text");
   cout << "Capacity s2 = " << s2.capacity() << " size = " << s2.size() << "  '"
        << s2 << "'" << endl
        << endl;

   s2.clear();
   cout << "Capacity s2 = " << s2.capacity() << " size = " << s2.size() << "  '"
        << s2 << "'" << endl
        << endl;

   // string::reserve(size_t n = 0) requests that the string capacity be
   // adapted to a planned change in size to a length of up to n characters.
   s2.reserve(5);
   cout << "Capacity s2 = " << s2.capacity() << " size = " << s2.size()
        << " max size = " << s2.max_size() << "  '" << s2 << "'" << endl
        << endl;

   return 0;
}
