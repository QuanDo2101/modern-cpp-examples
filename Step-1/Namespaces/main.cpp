#include <iostream>

// Global variables in different namespaces: a and b
// Coding style: do not indent code in namespaces

namespace a {
int i = 1;
double d = 1.1;
} // namespace a

namespace b {
int i = 2;
double d = 2.2;
} // namespace b

// ::  scope resolution operator

double d = 3.3; // Global variable

int main()
{
   double d = 4.4; // Local variable in main()

   std::cout << "a::i = " << a::i << "  b::i = " << b::i << std::endl;
   std::cout << "d = " << d << "  ::d =" << ::d << "  a::d = " << a::d
             << "  b::d = " << b::d << std::endl;
   std::cout << std::endl;

   return 0;
}
