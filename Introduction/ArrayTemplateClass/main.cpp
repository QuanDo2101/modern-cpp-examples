#include <array>
#include <iostream>

using namespace std;

int maxArray(const array<int, 4> &ar);

int main()
{
   array<int, 4> a1{-1, -2, 3, 4};
   array<int, 4> ar2{a1}; // copy ctor

   cout << "Max value in array a1 = " <<  maxArray(a1) << endl;

   cout << "All values in ar2 = "; 
   // Range based for loop
   for (auto data: ar2) {
       cout << data << " ";
   }
   cout << endl;

   return 0;
}

int maxArray(const array<int, 4> &ar)
{
   int max = ar[0];

   // some overhead: ar[0] already in max
   for (auto e : ar) {
      if (e > max) {
         max = e;
      }
   }

   return max;
}
