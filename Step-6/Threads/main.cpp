#include <iostream>
#include <thread>

const int N{10000};

void f1(char c)
{
   int i{N};
   while (i > 0) {
      std::cerr << c;
      --i;
   }
}

void f2(char c1, char c2)
{
   int i{N};
   while (i > 0) {
      std::cerr << c1 << c2;
      --i;
   }
}

class Functor3
{
public:
   Functor3(char c)
      : c_{c}
   {
   }
   // Overloaded function operator
   void operator()()
   {
      int i{N};
      while (i > 0) {
         std::cerr << c_;
         --i;
      }
   }

private:
   char c_;
};

// Compiler will generate default ctor, dtor, copy ctor and
// assignment operator.
class Other
{
public:
   void doit(char c1, char c2, char c3)
   {
      int i{N};
      while (i > 0) {
         std::cerr << c1 << c2 << c3;
         --i;
      }
   }
};

// Main thread
int main()
{
   const int N{::N};

   Other other4;

   // Local Lambda function
   auto f5 = [N](char c) {
      int i{N};
      while (i > 0) {
         std::cerr << c;
         --i;
      }
   };

   std::cout << "Hello World of threads!" << std::endl;

   // Worker threads
   std::thread th1{f1, 'o'};
   std::thread th2{f2, 'T', '2'};

   Functor3 ftr3{'f'};
   std::thread th3{ftr3};

   // Class member function
   std::thread th4{&Other::doit, &other4, 'a', ' ', 'c'};

   // lambda function
   std::thread th5{f5, 'L'};

   // Joining all threads
   th1.join();
   th2.join();
   th3.join();
   th4.join();
   th5.join();

   std::cout << "\nGoodbye World of threads!" << std::endl;

   return 0;
}
