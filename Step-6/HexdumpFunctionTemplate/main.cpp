#include <iomanip>
#include <iostream>
#include <type_traits>

// Templates are used in the C++ Standard Template Library (STL).
//
// Without templates you must implement this function in C++ for every 
// type by function overloading.
//
// void hexdump(int data) {
//   unsigned char* p = reinterpret_cast<unsigned char*>(&data);
//   for (size_t i = 0; i < sizeof(int); ++i) {
//      std::cout << std::setfill('0') << std::setw(2)
//                << std::hex << static_cast<int>(p[i]) << " ";
//   }
// }
//
// void hexdump(double data) {
//   unsigned char* p = reinterpret_cast<unsigned char*>(&data);
//   for (size_t i = 0; i < sizeof(double); ++i) {
//      std::cout << std::setfill('0') << std::setw(2)
//                << std::hex << static_cast<int>(p[i]) << " ";
//   }
// }
//

// Generic function
template <typename T>
void hexdump(T data)
{
   // Compile time checking, no pointer types allowed
   static_assert(not std::is_pointer<T>::value, "T can't be a pointer");

   unsigned char *p = reinterpret_cast<unsigned char *>(&data);
   for (size_t i = 0; i < sizeof(T); ++i) {
      std::cout << std::setfill('0') << std::setw(2) << std::hex
                << static_cast<int>(p[i]) << " ";
   }
}

using namespace std;

int main(int argc, char *argv[])
{
   cout << "Hello World of template functions!\n"
           "Several data types in bytes\n\n";

   cout << "long double 1.123 = ";
   hexdump(1.123L); // long double
   cout << endl;

   cout << "double      1.123 = ";
   hexdump(1.123); // double
   cout << endl;

   cout << "float       1.123 = ";
   hexdump(1.123f); // double
   cout << endl;

   cout << "long long      10 = ";
   hexdump(10LL); // long long
   cout << endl;

   cout << "long           10 = ";
   hexdump(10L); // long
   cout << endl;

   cout << "int            10 = ";
   hexdump(10); // int
   cout << endl;

   // hexdump("This is text"); //=> compile time error caused by static assert
   // cout << endl;

   cout << "char          'a' = ";
   hexdump('a'); // char
   cout << endl << endl;

   return 0;
}
