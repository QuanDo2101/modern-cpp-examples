#ifndef APPINFO_H
#define APPINFO_H

#define APPNAME "fccfMQTTcp"

#define MAJOR_VERSION "3"
#define MINOR_VERSION "4"
#define REVISION_VERSION "3"

#define VERSION MAJOR_VERSION "." MINOR_VERSION "." REVISION_VERSION
#define APPNAME_VERSION APPNAME " v" VERSION

#endif
