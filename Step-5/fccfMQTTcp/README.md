# Executing fccfMQTTcp

```plantuml
@startuml component
actor fccfMQTTcp <<MQTT client>>
node broker <<host>>

fccfMQTTcp<---right->broker
@enduml
```

Use the next commands and options to start the application. Some examples:

- host = 127.0.0.1 (default, local MQTT broker must be started)
- port = 1883 (default)

```bash
./fccfMQTTcp
```

- host = broker.hivemq.com
- port = 1883 (default)

```bash
./fccfMQTTcp broker.hivemq.com
```

- host = RPI2000.local  
- port = 5000

```bash
./fccfMQTTcp RPI2000.local 5000
```

After starting fccfMQTTcp will try to connect to the selected MQTT broker.

The application will show a lot of logging and debug information to stdout and stderr. This can be switched off by changing the related code.
