// RAII: Resource Acquisition Is Initialization.
// Dynamic memory allocation by new uses the HEAP.
//
// Necessary but not implemented, for a deep copy:
//    RawPointers(const RawPointers&)
//    operator=(const RawPointers&)

#include <iostream>
#include <memory>

class RawPointers
{
public:
   RawPointers() = default;
   ~RawPointers()
   {
      delete pInt_;
      delete pDouble_;
      std::cerr << "DTOR " << __func__ << " explicit deleted allocated memory"
                << std::endl;
   }

private:
   // raw pointers, naked pointers, C pointers
   // C++11 in-class initialization
   int *pInt_{new int{100}};
   double *pDouble_{new double{123.123}};
};

class SmartPointers
{
public:
   SmartPointers() = default;
   ~SmartPointers()
   {
      // No need to deallocate, dtor of unique_ptr will do this.
      std::cerr << "DTOR " << __func__ << " implicit deleted allocated memory"
                << std::endl;
   }

private:
   // C++11 in-class intialization
   std::unique_ptr<int> pInt_{std::make_unique<int>(10)};
   std::unique_ptr<double> pDouble_{std::make_unique<double>(20.20)};
};

int main()
{
   RawPointers rp;
   SmartPointers sp;

   {
      std::unique_ptr<RawPointers> pRP1{new RawPointers};
      std::cerr << "pRP1 = " << pRP1.get() << std::endl;
      // Unique pointers are moveable.
      // std::unique_ptr<RawPointers> pRP2{pRP1};  ==> compiler error
      std::unique_ptr<RawPointers> pRP2{move(pRP1)};

      std::cerr << "move(pRP1) = " << pRP1.get() << std::endl;
      std::cerr << "pRP2 = " << pRP2.get() << std::endl;
   }

   return 0;
}
