// fccfMQTT: MQTT client, temperature value converter
// Fahrenheit to Celcius and Celcius to Fahrenheit converter

#include "AppInfo.h"
#include "Config.h"
#include "TemperatureConverter.h"

#include <csignal>
#include <iostream>
#include <sstream>
#include <string>

std::string MosquittoLibVersion();

volatile sig_atomic_t receivedSIGINT{false};

/// Signal handling routine
/// \param s not used
void handleSIGINT(int /* s */)
{
   receivedSIGINT = true;
}

/// \warning Processing of the command line arguments is not robust.
/// No syntax checking implemented.
int main(int argc, char *argv[])
{
   try {
      std::string mqttBroker{MQTT_LOCAL_BROKER};
      int mqttBrokerPort{MQTT_LOCAL_BROKER_PORT};

      switch (argc) {
         case 1:
            // Using MQTT_LOCAL_BROKER and MQTT_LOCAL_BROKER_PORT
            break;
         case 2:
            // Using MQTT_LOCAL_BROKER_PORT
            mqttBroker = std::string(argv[1]);
            break;
         case 3:
            mqttBroker = std::string(argv[1]);
            mqttBrokerPort = std::stoi(argv[2]);
            break;
         default:
            std::cerr << "\n\nERROR command line arguments: "
                         "fccfMQTT <URL broker> <broker port>\n";
            exit(EXIT_FAILURE);
      }

      signal(SIGINT, handleSIGINT);

      std::cout << "-- MQTT application:\n   " << APPNAME_VERSION << "  ";

      mosqpp::lib_init();

      std::cout << "\n   uses Mosquitto lib version " << MosquittoLibVersion()
                << "\n";

      TemperatureConverter tempconv("fccf", "tempconv", mqttBroker,
                                    mqttBrokerPort);
      while (!receivedSIGINT) {
         int rc{tempconv.loop()};
         if (rc) {
            std::cerr << "-- MQTT reconnect rc = " << rc << " '"
                      << mosquitto_strerror(rc) << "'" << std::endl;
            tempconv.reconnect();
         }
      }
   }
   catch (std::invalid_argument &e) {
      std::cerr << "Exception invalid argument: " << e.what() << "\n";
   }
   catch (std::exception &e) {
      std::cerr << "Exception: " << e.what() << "\n";
   }
   /// \note catch(...): ... ellipsis, catches all unhandled unexpected
   /// exceptions.
   catch (...) {
      std::cerr << "UNKNOWN EXCEPTION \n";
   }

   std::cout << "-- MQTT application: " << APPNAME_VERSION << " stopped\n\n";

   mosqpp::lib_cleanup();

   return 0;
}

std::string MosquittoLibVersion()
{
   int major{0};
   int minor{0};
   int revision{0};
   mosqpp::lib_version(&major, &minor, &revision);

   std::stringstream s;
   s << major << '.' << minor << '.' << revision;

   return s.str();
}
