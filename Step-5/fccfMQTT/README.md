# Executing fccfMQTT

```plantuml
@startuml component
actor fccfMQTT <<MQTT client>>
node broker <<host>>

fccfMQTT<---right->broker
@enduml
```

Use the next commands and options to start the application. Some examples:

- host = 127.0.0.1 (default, local MQTT broker must be started)
- port = 1883 (default)

```bash
./fccfMQTT
```

- host = broker.hivemq.com
- port = 1883 (default)

```bash
./fccfMQTT broker.hivemq.com
```

- host = RPI2000.local  
- port = 5000

```bash
./fccfMQTT RPI2000.local 5000
```

After starting fccfMQTT will try to connect to the selected MQTT broker.

The application will show a lot of logging and debug information to stdout and stderr. This can be switched off by changing the related code.
