class A
{
public:
   int x;
   void f1();

protected:
   int y;
   void f2();

private:
   int z;
   void f3();
};

// Public inheritance, class A is the base class
class B : public A
{
   // x and f1() are public
   // y and f2() are protected
   // z and f3() are not accessible from B
};

// Protected inheritance, class A is the base class
class C : protected A
{
   // x and f1() are protected
   // y and f2() are protected
   // z and f3() are not accessible from C
};

// Private inheritance, class A is the base class
class D : private A // 'private' is default for classes
{
   // x and f1() are private
   // y and f2() are private
   // z and f3() are not accessible from D
};
