#include "PIcontroller.h"

PIcontroller::PIcontroller(double Tsample, double P, double I)
   : Controller{Tsample}
   , P_{P}
   , I_{I}
   , integrator_{0.0}
{
}

/// @warning Implementation PI control function correct?
double PIcontroller::operator()(double err)
{
   // Euler integration
   integrator_ += I_ * err;
   return P_ * err + integrator_;
}
