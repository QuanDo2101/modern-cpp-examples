#include "Controller.h"

int Controller::count_ = 0;

Controller::Controller(double Tsample)
   : Tsample_{Tsample}
{
   ++count_;
}

Controller::~Controller()
{
   --count_;
}
