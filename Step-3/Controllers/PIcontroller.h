#ifndef PICONTROLLER_H
#define PICONTROLLER_H

#include "Controller.h"

/// PIcontroller represent a Proportional Integral controller.
/// PIcontroller is_a Controller.
class PIcontroller : public Controller
{
public:
   PIcontroller(double Tsample, double P, double I);
   virtual ~PIcontroller() = default;
   
   // C++03 virtual double operator()(double err);
   // C++11 virtual double operator()(double err) override;
   double operator()(double err) override;
   // Inline function, defined in the class.
   void reset() { integrator_ = 0; }

private:
   double P_;
   double I_;
   double integrator_;
};

#endif // PICONTROLLER_H
