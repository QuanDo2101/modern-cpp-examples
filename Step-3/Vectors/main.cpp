// C++ STL container class: std::vector
// Array semantics: index operator []

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

// template function: T is template parameter
template <typename T>
void showVector(const std::vector<T> &v)
{
   for (auto &e : v) {
      std::cout << e << " ";
   }
   std::cout << std::endl;
}

int main()
{
   std::vector<int> v1{{1, 2, 3, 4, 5}};

   std::cout << v1[0] << " " << v1[3] << "\n";

   std::cout << "v1 = ";
   showVector(v1);
   v1.push_back(100);
   v1.push_back(200);
   std::cout << "v1 = ";
   showVector(v1);
   std::cout << "v1.capacity() = " << v1.capacity() << std::endl;

   std::vector<int> v2{v1}; // copy constructor
   std::cout << "v2 = ";
   showVector(v2);

   v2 = {-3, -4, -5}; // intializer list {....}
   std::cout << "v2 = ";
   showVector(v2);

   v1 = v2; // assignment operator =
   std::cout << "v1 = ";
   showVector(v1);
   std::cout << "v1.capacity() = " << v1.capacity() << std::endl;

   v2[1] = 1000;
   std::cout << "v2 = ";
   showVector(v2);

   // --------------------------------- auto C++14
   std::for_each(begin(v1), end(v1), [](auto &e) { e++; });
   std::cout << "v1 = ";
   showVector(v1);

   std::vector<int> v3(v2.size()); // create sufficient elements
   std::cout << "v3.capacity() = " << v3.capacity() << std::endl;
   std::cout << "v3 = ";
   showVector(v3);

   std::cout << "v2 = ";
   showVector(v2);

   // Copy v2 to v3 if condition is true, elements should be available in v3
   std::copy_if(begin(v2), end(v2), begin(v3), [](auto e) { return (e < 0); });

   std::cout << "v3 = ";
   showVector(v3);

   v3.erase(begin(v3) + 1);
   std::cout << "v3 = ";
   showVector(v3);

   v3.erase(end(v3) - 1);
   std::cout << "v3 = ";
   showVector(v3);

   return 0;
}
