#include <functional>
#include <iostream>
#include <string>
#include <vector>

// Class template std::function is a general-purpose polymorphic function
// wrapper. Instances of std::function can store, copy, and invoke any Callable
// target -- functions, lambda expressions, bind expressions, or other
// function objects, as well as pointers to member functions and pointers
// to data members.

// The stored callable object is called the target of std::function.
// If a std::function contains no target, it is called empty.
// Invoking the target of an empty std::function results in
// std::bad_function_call exception being thrown.

using CommandFunction = std::function<int(const std::string &)>;
using Program = std::vector<CommandFunction>;

// Global function
int cmnd1(const std::string &str)
{
   std::cout << "Function: " << str << std::endl;
   return 100;
}

// Functor, class contains the overloaded function call operator.
// A functor can contain data members.
class Cmnd2
{
public:
   Cmnd2(int data)
      : data_(data)
   {
   }

   int operator()(const std::string &str)
   {
      std::cout << "Functor: " << str << " " << data_ << std::endl;
      return data_;
   }

private:
   int data_;
};

int cmnd3(const std::string &str, int extra)
{
   std::cout << "Function extra parameter: " << str << " " << extra
             << std::endl;
   return extra;
}

// Executes all commands in program
void execute(const Program &program)
{
   for (const auto &cmd : program) {
      // Every command has "Hello command" as input string (first argument)
      std::cout << cmd("Hello command") << std::endl << std::endl;
   }
}

int main()
{
   int someData = 123100;

   // Local lambda function.
   auto cmnd4 = [someData](const std::string &str) {
      std::cout << "Lambda function: " << str << " " << someData << std::endl;
      return someData;
   };

   Program program;

   Cmnd2 cmnd2{4};

   // Put commands in program
   program.push_back(cmnd1);
   program.push_back(cmnd2);
   // Create anonymous Cmnd2 object
   program.push_back(Cmnd2{1000});
    // Create second anonymous Cmnd2 object
   program.push_back(Cmnd2{3333});
   // next bind() creates function object, when called, calls cmnd3 with 
   // arguments bound to argument list.
   // argument 1 cmnd3: std::placeholders::_1  == "Hello command", see execute()
   // argument 2 cmnd3: int extra == 3000
   program.push_back(std::bind(cmnd3, std::placeholders::_1, 3000));
   program.push_back(cmnd4);

   // Execute all commands in program
   execute(program);

   return 0;
}
