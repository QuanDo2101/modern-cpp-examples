// RaspberyPi and SenseHAT application

#include "AppInfo.h"
#include "SenseHAT.h"

#include <iomanip>
#include <iostream>
#include <string>

/// Shows the measured temperature, pressure and huminity values.
int main(int argc, char *argv[])
{
   try {
      std::cout << "-- Application: " << APPNAME_VERSION << " started\n\n";

      SenseHAT sensehat;
      sensehat.leds.clear();

      std::cout << "-- Pressure    = " << sensehat.get_pressure() << std::endl;
      std::cout << "-- Humidity    = " << sensehat.get_humidity() << std::endl;
      std::cout << "-- Temperature = "
                << sensehat.get_temperature_from_humidity() << std::endl;
      std::cout << "-- Temperature = "
                << sensehat.get_temperature_from_pressure() << std::endl;
      std::cout << "-- Temperature = " << sensehat.get_temperature()
                << std::endl;

      std::cout << "-- Press a key ...";
      std::cin.get();
   }
   catch (std::exception &e) {
      std::cerr << "-- Exception " << e.what() << std::endl;
   }
   catch (...) {
      std::cerr << "-- UNKNOWN EXCEPTION\n";
   }

   std::cout << "\n-- Application: " << APPNAME_VERSION << " stopped"
             << std::endl
             << std::endl;

   return 0;
}
