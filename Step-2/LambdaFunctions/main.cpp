// Lambda functions and algorithms

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

void updateData(vector<double> &v);

int main()
{
   // local function or closure in main(),
   //      [] capture list, () parameter list
   // auto: compiler deduces the type of a declared variable from its
   // initialization expression.
   auto lf1 = [](int a, int b) { return a + b; };

   cout << lf1(2, -4) << endl;

   const int C = 1000;
   auto lf2 = [C](int a, int b) { return a + b + C; };

   cout << lf2(2, -4) << endl;
   cout << lf2(-2, 4) << endl << endl;

   vector<double> data{-3, 0.0, -2.1, 4.3};

   cout << "data = ";
   for (auto d : data) {
      cout << d << " ";
   }
   cout << endl;

   cout << "minimal element in data = " << *min_element(begin(data), end(data))
        << endl;
   cout << "maximal element in data = " << *max_element(begin(data), end(data))
        << endl;

   updateData(data);
   cout << "transformed data = ";
   for (auto d : data) {
      cout << d << " ";
   }
   cout << endl << endl;

   string hello{"Hello Lambda functions"};
   cout << "hello = '" << hello << "'" << endl;

   cout << "counted 'a' in hello = " << count(begin(hello), end(hello), 'a')
        << endl;

   auto fsort = [](string &str) { sort(begin(str), end(str)); };
   fsort(hello);
   cout << "Sorted hello: '" << hello << "'" << endl;

   // Value for index if not available: string::npos
   cout << "string::npos = " << string::npos << "  == -1  " << size_t(-1)
        << endl;

   // string containing the chars for the change to upper case
   const string toUpper{"abde"};

   auto ftoUpper = [toUpper](string &str) {
      for (auto &c : str) {
         if (toUpper.find(c) != str.npos) {
            c = toupper(c);
         }
      }
   };

   ftoUpper(hello);
   cout << "Processed hello: '" << hello << "'" << endl;

   return 0;
}

void updateData(vector<double> &v)
{
   // transform algorithm, applies an operation sequentially to all the elements
   // of a container, the results are here stored in the same container
   transform(begin(v), end(v), begin(v),
             // operation to an element of container object
             // [](double d) { return d < 0.01 ? 0 : d; });
             [](auto d) { return d < 0.01 ? 0 : d; });
}
