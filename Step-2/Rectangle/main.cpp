#include "Rectangle.h"

#include <iostream>

using namespace std;

int main()
{
   Rectangle r1;
   Rectangle r2{1.1, 3.1}; // Uniform initialization: { ..... }

   cout << " r1 = " << r1 << endl;
   cout << " r2 = " << r2 << endl;
   cout << "Enter for r1 width and height: ";
   cin >> r1;
   cout << " r1 = " << r1 << endl;

   r1.setWidth(r2.getWidth() * r2.getWidth());
   cout << " r1 = " << r1 << endl;
   cout << " Surface area r2 = " << r2.surfaceArea() << endl;

   return 0;
}
