// Tokenizer, text processing: breaking up a string into a sequence of strings,
// such as words, keywords, phrases, symbols and other elements.
// This program uses C++11

#include "tokenizer.h"
#include <algorithm>
#include <iostream>

// Callback function, uses no chars but ints!
// C functions for character classification, ISO/IEC 9899:TC2 7.4/1
int isForwardSlash(int c)
{
   return (c == '/');
}

// Default delimiter function can be found in tokenizer.h
std::vector<std::string> split(const std::string &str, int delimiter(int))
{
   std::vector<std::string> result;
   // Const iterators to str
   auto b = std::cbegin(str);
   auto e = std::cend(str);

   while (b != e) {
      // find first char != delimiter, using an C++ algorithm find_if_not
      b = std::find_if_not(b, e, delimiter);
      if (b != e) {
         // find first char == delimiter, using an C++ algorithm find_if
         auto i = std::find_if(b, e, delimiter);
         // substring between b and i added to result
         result.push_back(std::string(b, i));
         b = i;
      }
   }

   return result;
}

// Default delimiter function can be found in tokenizer.h
std::vector<std::string> splitLog(const std::string &str, int delimiter(int))
{
   std::vector<std::string> result;
   // Const iterators to str
   auto b = std::cbegin(str);
   auto e = std::cend(str);

   std::cout << "str b --> e = '" << std::string(b, e) << "'\n\n";

   while (b != e) {
      // find first char != delimiter, using an C++ algorithm find_if_not
      b = std::find_if_not(b, e, delimiter);

      if (b != e) {
         std::cout << "str b --> e = '" << std::string(b, e) << "'\n";

         // find first char == delimiter, using an C++ algorithm find_if
         auto i = std::find_if(b, e, delimiter);
         std::cout << "str b --> i = '" << std::string(b, i) << "'\n";
         std::cout << "str i --> e = '" << std::string(i, e) << "'\n\n";

         // substring between b and i added to result
         result.push_back(std::string(b, i));
         b = i;
      }
   }

   return result;
}
