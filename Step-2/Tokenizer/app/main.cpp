#include "tokenizer.h"
#include <iostream>

const int nCHARS = 60;

int main()
{
   // String containing ints and doubles
   // Uniform initialisation
   std::string line{" 1  2   33.337  "};
   std::cout << "\nInput = '" << line << "'\n";

   // Using default delimiter: isspace (see tokenizer.h)
   std::vector<std::string> tokens{split(line)};

   for (const auto &tkn : tokens) {
      std::cout << tkn << std::endl;
   }
   std::cout << std::endl;

   // Conversion of string tokens to int and double types
   // Uniform initialisation
   int i1{stoi(tokens[0])};
   int i2{stoi(tokens[1])};
   double d3{stod(tokens[2])};

   std::cout << "Converted strings: " << i1 << " " << i2 << " " << d3
             << std::endl
             << std::string(nCHARS, '-') << "\n\n";

   std::cout << "Log split():\n\n";
   splitLog(line);
   std::cout << std::string(nCHARS, '-') << "\n\n";

   // Example: using a MQTT topic string (is like a directory path)
   std::string topic{"ESEiot/1920feb/RPI123456/temperature"};

   std::cout << "Topic = '" << topic << "'\n\n";
   std::vector<std::string> tokensTopic{split(topic, isForwardSlash)};

   for (const auto &tkn : tokensTopic) {
      std::cout << tkn << std::endl;
   }
   std::cout << std::endl
             << tokensTopic[0] << "  " << tokensTopic[1] << "  "
             << tokensTopic[2] << "\n"
             << std::string(nCHARS, '-') << "\n\n";

   std::cout << "Log split():\n\n";
   splitLog(topic, isForwardSlash);
   std::cout << std::string(nCHARS, '-') << "\n\n";

   return 0;
}
