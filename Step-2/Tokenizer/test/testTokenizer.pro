TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += c++17

INCLUDEPATH += $$PWD/../app
INCLUDEPATH += $$PWD/Catch2
DEPENDPATH += $$PWD/../app

SOURCES += testMain.cpp \
   testTokenizer.cpp

win32: CONFIG(debug, debug|release) {
   OBJECTS += $$OUT_PWD/../app/debug/tokenizer.o
}
win32: CONFIG(release, debug|release) {
   OBJECTS += $$OUT_PWD/../app/release/tokenizer.o
}

unix: OBJECTS += $$OUT_PWD/../app/tokenizer.o
