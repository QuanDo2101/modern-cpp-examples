// Unit tests: using C++ Catch2 unit test framework

#include "catch.hpp"
#include "tokenizer.h"

#include <iostream>
#include <string>

TEST_CASE("Tokenizer: split() function")
{
   SECTION("Test empty input string")
   {
      std::string str;
      std::vector<std::string> result{split(str)};

      REQUIRE(result.empty());
   }

   SECTION("Test input string with 3 sub strings, separator char: space ")
   {
      std::string str{" 111  2222     33333"};
      std::vector<std::string> result{split(str)};

      REQUIRE(result.size() == 3);
      REQUIRE(result[0] == "111");
      REQUIRE(result[1] == "2222");
      REQUIRE(result[2] == "33333");
   }

   SECTION("Test input string with 4 sub strings, separator char: ,")
   {
      auto sp = [](int c) { return static_cast<int>(c == ','); };
      std::string str{"1,22,333,4444"};
      std::vector<std::string> result{split(str, sp)};

      REQUIRE(result.size() == 4);
      REQUIRE(result[0] == "1");
      REQUIRE(result[1] == "22");
      REQUIRE(result[2] == "333");
      REQUIRE(result[3] == "4444");
   }
}
