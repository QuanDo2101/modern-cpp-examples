#!/usr/bin/python

from  sense_hat import SenseHat
import time
import socket

show = True

def get_ip_address():
   ip_address = ''
   s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
   try:
       s.connect(("8.8.8.8", 80))
       ip_address = s.getsockname()[0]
       s.close()
   except Exception, e:
       ip_address = 'not connected'
   return ip_address


def stop():
   global show
   show = False


#-------------------------------------------------------------------------------

sensehat = SenseHat()
sensehat.stick.direction_middle = stop

speed = 0.08
maxRGB = 100
hostname = socket.gethostname()
rgbLightYellow = (50, 50, 30)

while show:
 	sensehat.show_message(hostname + ' ' + get_ip_address(), speed, (0, maxRGB, maxRGB))
	time.sleep(0.5)

sensehat.set_rotation(0)
sensehat.clear(rgbLightYellow)

